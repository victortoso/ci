Name: $PKG_NAME
Version: $PKG_VERSION
Release: $TIMESTAMP$PKG_RELEASE%{?dist}

Summary: USB network redirection protocol libraries

Group: System Environment/Libraries
License: LGPLv2+
URL: https://www.spice-space.org/

Source0: $PKG_ARCHIVE

BuildRequires:  libusb1-devel >= 1.0.9

%description
The usbredir libraries allow USB devices to be used on remote and/or virtual
hosts over TCP.  The following libraries are provided:

usbredirparser:
A library containing the parser for the usbredir protocol

usbredirhost:
A library implementing the USB host side of a usbredir connection.
All that an application wishing to implement a USB host needs to do is:
* Provide a libusb device handle for the device
* Provide write and read callbacks for the actual transport of usbredir data
* Monitor for usbredir and libusb read/write events and call their handlers


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%package        server
Summary:        Simple USB host TCP server
Group:          System Environment/Daemons
License:        GPLv2+
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    server
A simple USB host TCP server, using libusbredirhost.


%prep
%setup -q -n $PKG_ARCHIVE_NO_EXTENSION


%build
%configure --disable-static
make %{?_smp_mflags} V=1


%install
%make_install
rm \$RPM_BUILD_ROOT%{_libdir}/libusbredir*.la


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%{!?_licensedir:%global license %%doc}
%license COPYING.LIB
%{_libdir}/libusbredir*.so.*

%files devel
%doc usb-redirection-protocol.txt README.multi-thread ChangeLog TODO
%{_includedir}/usbredir*.h
%{_libdir}/libusbredir*.so
%{_libdir}/pkgconfig/libusbredir*.pc

%files server
%{!?_licensedir:%global license %%doc}
%license COPYING
%{_sbindir}/usbredirserver
%{_mandir}/man1/usbredirserver.1*
